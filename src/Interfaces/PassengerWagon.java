package Interfaces;

import java.util.ArrayList;
import java.util.List;

public abstract class PassengerWagon implements PassengerCar {

    private List<Passenger> passengers = new ArrayList<>();

    @Override
    public void addPassenger(Passenger passenger) throws Exception {
        if (isFull()) throw new Exception("IWagon is full");
        passengers.add(passenger);
    }

    @Override
    public int getPassengerCount() {
        return passengers.size();
    }

    @Override
    public boolean isFull() {
        return passengers.size() == getPlaceCount();
    }
}
