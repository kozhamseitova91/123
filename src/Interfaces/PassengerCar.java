package Interfaces;

public interface PassengerCar extends Car {
    void addPassenger(Passenger passenger) throws Exception;
    int getPassengerCount();
    int getPlaceCount();
    boolean isFull();
}
