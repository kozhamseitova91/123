package Interfaces;

public interface Passenger {
    String getName();
    int getPlaceNumber();
}
