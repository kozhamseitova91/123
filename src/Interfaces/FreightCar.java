package Interfaces;

public interface FreightCar extends Car {
    int getMaxWeight();
    void setMaxWeight();
}
