package Interfaces;

import java.util.List;

public interface Train {
    void startMoving();
    void stopMoving();
    String getDestination();
    void setDestination(String destination);

    List<Car> getCars();
    void addCar(Car car);

}
