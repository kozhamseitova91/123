package Interfaces;

public class LuxCar extends PassengerWagon {

    private final int placeCount = 10;

    @Override
    public int getPlaceCount() {
        return placeCount;
    }

    @Override
    public String getType() {
        return "Lux Class wagon";
    }
}
