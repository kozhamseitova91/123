package com.company;

import Interfaces.BaiterekTrain;
import Interfaces.EconomCar;
import Interfaces.LuxCar;

public class Main {

    public static void main(String[] args) {
        BaiterekTrain train = new BaiterekTrain();
        train.addCar(new EconomCar());
        train.addCar(new EconomCar());
        train.addCar(new LuxCar());

        train.getCars().forEach(c -> System.out.println(c.getType()));
    }
}
